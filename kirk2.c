#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdbool.h>
#include <ctype.h>

// Message buffer, containing a type and string message.
struct my_msgbuf
{
	long mtype;
	char mtext[200];
};

int main(void)
{
	// Instansiate message buffer, message queue ID and key.
	struct my_msgbuf buf;
	int msqid;
	key_t key;

	// Generate a key, if erroneous, print error and exit.
	if ((key = ftok("kirk2.c", 'B')) == -1) 
	{
		perror("ftok");
		exit(1);
	}

	// Get message queue ID based, if erroneous, print error and exit.
	if ((msqid = msgget(key, 0644 | IPC_CREAT)) == -1) 
	{
		perror("msgget");
		exit(1);
	}
	
	// Print an entry message to the user.
	printf("Enter lines of text, ^D to quit:\n");

	// While the command line input is not null, i.e. keep looping if there is a input on the command line.
	while(fgets(buf.mtext, sizeof buf.mtext, stdin) != NULL) 
	{
		// Length of the input string.
		int len = strlen(buf.mtext);

		// Ditch newline character: '\n'
		if (buf.mtext[len-1] == '\n') buf.mtext[len-1] = '\0';

		// Presumes a message is urgent...
		bool urg = true;
		// Loops through all the characters in the input string.
		int j;
		for (j = 0; j < len - 1; ++j)
		{
			// Sets current character being investigated.
			char c = buf.mtext[j];
			// ... unless there is a lower case character ...
			if (islower(c))
			{
				// ... sets urgent to false ...
				urg = false;
				// ... and breaks the loop.
				break;
			}
		}

		// If urgent, sets the mtype to two, which only starfleet reads.
		if (urg) buf.mtype = 2;
		// Else, sets the mtype to one, which only spock reads.
		else buf.mtype = 1;

		// Finally, sends the message to the message queue.
		msgsnd(msqid, &buf, len+1, 0);
	}

	// Remove the message queue.
	if (msgctl(msqid, IPC_RMID, NULL) == -1) 
	{
		perror("msgctl");
		exit(1);
	}

	return 0;
}

