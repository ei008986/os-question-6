#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

// Message buffer, containing a type and string message.
struct my_msgbuf 
{
	long mtype;
	char mtext[200];
};

int main(void)
{
	// Instansiate message buffer, message queue ID and key.
	struct my_msgbuf buf;
	int msqid;
	key_t key;

	// Generate a key, if erroneous, print error and exit.
	if ((key = ftok("kirk2.c", 'B')) == -1) 
	{
		perror("ftok");
		exit(1);
	}

	// Get message queue ID based, if erroneous, print error and exit.
	if ((msqid = msgget(key, 0644)) == -1) 
	{
		perror("msgget");
		exit(1);
	}
	
	// Print welcome message to user.
	printf("spock2: ready to receive messages, captain.\n");

	// Endless loop
	for(;;) 
	{
		// Recieve message, but only if the type is equal to 1 (non urgent).
		if (msgrcv(msqid, &buf, sizeof buf.mtext, 1, 0) == -1) 
		{
			perror("msgrcv");
			exit(1);
		}
		// Then print the message.
		printf("spock2: \"%s\"\n", buf.mtext);
	}

	return 0;
}

